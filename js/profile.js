(function(){
    // timetable navigation
    $('.profile-nav .switch-nav .edit').on('click', function () {
        
        $('.profile-nav .switch-nav .switch-button').css('transform', 'translate3D(215px, 0, 0)').css('transition', 'all 0.2s');
        
        $(this).css('color', '#FFFFFF').css('transition', 'all 0.2s');
        $('.profile-nav .switch-nav .overview').css('color', '#3D82F0').css('transition', 'all 0.2s');
        $('.information-content').slideUp(80);
        $('.edit-content').slideDown(80);
        $('.your-plan').slideUp(40);
    });
    
    $('.profile-nav .switch-nav .overview').on('click', function () {
        $('.profile-nav .switch-nav .switch-button').css('transform', 'translate3D(0, 0, 0)').css('transition', 'all 0.2s');
        $(this).css('color', '#FFFFFF').css('transition', 'all 0.2s');
        $('.profile-nav .switch-nav .edit').css('color', '#3D82F0').css('transition', 'all 0.2s');
        $('.information-content').slideDown(80);
        $('.edit-content').slideUp(80);
        $('.your-plan').slideDown(10);
    });

    let l=true;

    $('.edit-content .login-details .show').click(()=>{
        if(l==true){
            $('.show').css('background', 'url(assets/images/hide.svg)');
            l=false;
            $('.user-password').attr('type', 'text');
        }else{
            $('.show').css('background', 'url(assets/images/show.svg)');
            l=true;
            $('.user-password').attr('type', 'password');
        }
    });

    
    autosize($('textarea'));

    if($('.edit-content .connect-with').find('a span').eq(0).text() === 'Connected') {
        $('.edit-content .connect-with').find('a').eq(0).mouseenter(function() {
            $('.edit-content .connect-with').find('a').eq(0).find('span').text('Disconnect');
        }).mouseleave(function() {
            $('.edit-content .connect-with').find('a').eq(0).find('span').text('Connected');
        });
    }

    if($('.edit-content .connect-with').find('a span').eq(1).text() === 'Connected') {
        $('.edit-content .connect-with').find('a').eq(1).mouseenter(function() {
            $('.edit-content .connect-with').find('a').eq(1).find('span').text('Disconnect');
        }).mouseleave(function() {
            $('.edit-content .connect-with').find('a').eq(1).find('span').text('Connected');
        });
    }


    var url = window.location.href;
    if (url.includes("#edit")) {
        $('.profile-nav .switch-nav .switch-button').css('transform', 'translate3D(215px, 0, 0)').css('transition', 'all 0.2s');
        
        $(this).css('color', '#FFFFFF').css('transition', 'all 0.2s');
        $('.profile-nav .switch-nav .overview').css('color', '#3D82F0').css('transition', 'all 0.2s');
        $('.profile-nav .switch-nav .edit').css('color', '#ffffff').css('transition', 'all 0.2s');
        $('.information-content').slideUp(80);
        $('.edit-content').slideDown(80);
        $('.your-plan').slideUp(40);
    }

})();