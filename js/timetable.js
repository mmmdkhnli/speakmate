(function(){
// timetable navigation
$('.timetable-nav .switch-nav .advanced').on('click', function () {
    
    $('.timetable-nav .switch-nav .switch-button').css('transform', 'translate3D(215px, 0, 0)').css('transition', 'all 0.2s');
    
    $(this).css('color', '#FFFFFF').css('transition', 'all 0.2s');
    $('.timetable-nav .switch-nav .beginner').css('color', '#3D82F0').css('transition', 'all 0.2s');
    $('.timetable-nav .info #text-1').hide();
    $('.timetable-nav .info #text-2').show();
    $('.t-side').show().css('display', 'flex');
    $('.beginner-timetable').slideUp(80);
    $('.advanced-timetable').slideDown(80);
});

$('.timetable-nav .switch-nav .beginner').on('click', function () {
    $('.timetable-nav .switch-nav .switch-button').css('transform', 'translate3D(0, 0, 0)').css('transition', 'all 0.2s');
    $(this).css('color', '#FFFFFF').css('transition', 'all 0.2s');
    $('.timetable-nav .switch-nav .advanced').css('color', '#3D82F0').css('transition', 'all 0.2s');
    $('.timetable-nav .info #text-1').show();
    $('.timetable-nav .info #text-2').hide();
    $('.t-side').hide();
    $('.beginner-timetable').slideDown(80);
    $('.advanced-timetable').slideUp(80);
});

$('.timetable-nav .info #text-1 span').on('click', function () {
    $('.timetable-nav .switch-nav .switch-button').css('transform', 'translate3D(215px, 0, 0)').css('transition', 'all 0.2s');
    $('.timetable-nav .switch-nav .advanced').css('color', '#FFFFFF').css('transition', 'all 0.2s');
    $('.timetable-nav .switch-nav .beginner').css('color', '#3D82F0').css('transition', 'all 0.2s');
    $('.timetable-nav .info #text-1').hide();
    $('.timetable-nav .info #text-2').show();
    $('.t-side').show().css('display', 'flex');
    $('.beginner-timetable').slideUp(80);
    $('.advanced-timetable').slideDown(80);
});

$('.timetable-nav .info #text-2 span').on('click', function () {
    $('.timetable-nav .switch-nav .switch-button').css('transform', 'translate3D(0, 0, 0)').css('transition', 'all 0.2s');
    $('.timetable-nav .switch-nav .beginner').css('color', '#FFFFFF').css('transition', 'all 0.2s');
    $('.timetable-nav .switch-nav .advanced').css('color', '#3D82F0').css('transition', 'all 0.2s');
    $('.timetable-nav .info #text-1').show();
    $('.timetable-nav .info #text-2').hide();
    $('.t-side').hide();
    $('.beginner-timetable').slideDown(80);
    $('.advanced-timetable').slideUp(80);
});

// timetable video-content
$('.beginner-timetable .video-content .content-title').on('click', function () {
    $(this).find('.plus').toggleClass('minus').css('transition', 'all 0.1s');
    $(this).parent().find('.content-inner').slideToggle(200);
});

// timetable question content
$('.advanced-timetable .video-content .lesson .questions .question-title').on('click', function () {
    $(this).parent().find('.question-item').slideToggle(200);
    $(this).find('.select-vector').toggleClass('opened-vector');
});

// cc-items
if($('#desktop .inner').find('>a').length === 5) {
    $('.conversation-content .inner-item').css('width', '200px').css('height', '210px');
    $('.conversation-content .inner-item .flag').css('width', '75px').css('height', '75px');
    $('.soon .coming-soon').css('width', '75px').css('height', '75px');
    $('.conversation-lang').css('font-size', '18px').css('margin-top', '23.5px');
} 

if($('#desktop .inner').find('>a').length === 6) {
    $('.conversation-content .inner-item').css('width', '175px').css('height', '185px');
    $('.conversation-content .inner-item .flag').css('width', '70px').css('height', '70px');
    $('.soon .coming-soon').css('width', '70px').css('height', '70px');
    $('.conversation-lang').css('font-size', '17px').css('margin-top', '22.5px');
} 

if($('#desktop .inner').find('>a').length === 7) {
    $('.conversation-content .inner-item').css('width', '150px').css('height', '158px');
    $('.conversation-content .inner-item .flag').css('width', '60px').css('height', '60px');
    $('.soon .coming-soon').css('width', '60px').css('height', '60px');
    $('.conversation-lang').css('font-size', '15px').css('margin-top', '21.5px');
} 

if($('#desktop .inner').find('>a').length === 8) {
    $('.conversation-content .inner-item').css('width', '130px').css('height', '137px');
    $('.conversation-content .inner-item .flag').css('width', '50px').css('height', '50px');
    $('.soon .coming-soon').css('width', '50px').css('height', '50px');
    $('.conversation-lang').css('font-size', '13.5px').css('margin-top', '20.5px');
} 

if($('#desktop .inner').find('>a').length === 9) {
    $('.conversation-content .inner-item').css('width', '118px').css('height', '124px');
    $('.conversation-content .inner-item .flag').css('width', '43px').css('height', '43px');
    $('.soon .coming-soon').css('width', '43px').css('height', '43px');
    $('.conversation-lang').css('font-size', '12.5px').css('margin-top', '18.5px');
} 

if($('#desktop .inner').find('>a').length === 10) {
    $('.conversation-content .inner-item').css('width', '100px').css('height', '108px');
    $('.conversation-content .inner-item .flag').css('width', '38px').css('height', '38px');
    $('.soon .coming-soon').css('width', '38px').css('height', '38px');
    $('.conversation-lang').css('font-size', '12px').css('margin-top', '16.5px');
} 

})();