$(document).ready(function(){
    let sh_login_click=true;

    $('.login-popup .show').click(()=>{
        if(sh_login_click==true){
            $('.show').css('background', 'url(assets/images/hide.svg)');
            sh_login_click=false;
            $('.user-password').attr('type', 'text');
        }else{
            $('.show').css('background', 'url(assets/images/show.svg)');
            sh_login_click=true;
            $('.user-password').attr('type', 'password');
        }
    });

    $('.sign-popup .show').click(()=>{
        if(sh_login_click==true){
            $('.show').css('background', 'url(assets/images/hide.svg)');
            sh_login_click=false;
            $('.user-password').attr('type', 'text');
        }else{
            $('.show').css('background', 'url(assets/images/show.svg)');
            sh_login_click=true;
            $('.user-password').attr('type', 'password');
        }
    });

    $('.reset-popup .show').click(()=>{
        if(sh_login_click==true){
            $('.show').css('background', 'url(assets/images/hide.svg)');
            sh_login_click=false;
            $('.new-password').attr('type', 'text');
        }else{
            $('.show').css('background', 'url(assets/images/show.svg)');
            sh_login_click=true;
            $('.new-password').attr('type', 'password');
        }
    });
});
