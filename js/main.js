(function(){
$(function(){
var check = true;
// Lang-bar Control Pop-up
$('.lang-bar').on('click', function() {
    $('.lang-bar ul').fadeToggle(80);
    if(check === true) {
        $('.lang-bar span').css('color', '#03A9F4').css('transition', '0.08s');
        $(this).find('.down').removeClass('down').addClass('bottom').css('transform', 'rotate(180deg)').css('transition', '0.08s');
        check = false;
    } else{
        $('.lang-bar span').css('color', '#000000').css('transition', '0.08s');
        $(this).find('.bottom').removeClass('bottom').addClass('down').css('transform', 'rotate(0)').css('transition', '0.08s');
        check = true;
    }
});

$(document).on('click', function(e) {
    var target = e.target;

    if (!$(target).is('.lang-bar') && !$(target).parents().is('.lang-bar')) {
        $('.lang-bar ul').fadeOut(80);
        $('.lang-bar span').css('color', '#000000').css('transition', '0.08s');
        $('.lang-bar .bottom').removeClass('bottom').addClass('down').css('transform', 'rotate(0)').css('transition', '0.08s');
        check = true;
    }
})

// After-login Control Pop-up
$('.navbar .menu .down').on('click', function() {
    $('.navbar .menu ul').fadeToggle(80);
    $(this).toggleClass('up').css('transition', 'all 0.08s');
});

$(document).on('click', function(e) {
    var target = e.target;

    if (!$(target).is('.navbar .menu') && !$(target).parents().is('.navbar .menu')) {
        $('.navbar .menu .up').removeClass('up').addClass('down');
        $('.navbar .menu ul').fadeOut(80);
    }
})

var afterLoginMenuWidth  = $('.navbar .user').outerWidth() + $('.navbar .menu').outerWidth();
$('.navbar .menu ul').css('width', afterLoginMenuWidth + 24 + 'px');

if($('.navbar .count').text() >= 1) {
    $('.navbar .count').css('display', 'flex');
} else {
    $('.navbar .count').css('display', 'none');
}

if($('.navbar .count').text() >= 1) {
    $('.navbar .count').css('display', 'flex');
} else {
    $('.navbar .count').css('display', 'none');
}

// mobile notification par.
if($('.navbar .mobile-notify .mobile-message .count').text() >= 1) {
    $('.navbar .mobile-notify .mobile-message .count').css('display', 'flex');
} else {
    $('.navbar .mobile-notify .mobile-message .count').css('display', 'none');
}

if($('.navbar .mobile-notify .mobile-notification .count').text() >= 1) {
    $('.navbar .mobile-notify .mobile-notification .count').css('display', 'flex');
} else {
    $('.navbar .mobile-notify .mobile-notification .count').css('display', 'none');
}

// Converstaion flag's par.
var flagInner = '<div class="coming-soon"><div>';
$('.soon .flag').html(flagInner);

// cc-items
if($('#desktop .inner').find('>a').length === 5) {
    $('.conversation-content .inner-item').css('width', '200px').css('height', '210px');
    $('.conversation-content .inner-item .flag').css('width', '75px').css('height', '75px');
    $('.soon .coming-soon').css('width', '75px').css('height', '75px');
    $('.conversation-lang').css('font-size', '18px').css('margin-top', '23.5px');
} 

if($('#desktop .inner').find('>a').length === 6) {
    $('.conversation-content .inner-item').css('width', '175px').css('height', '185px');
    $('.conversation-content .inner-item .flag').css('width', '70px').css('height', '70px');
    $('.soon .coming-soon').css('width', '70px').css('height', '70px');
    $('.conversation-lang').css('font-size', '17px').css('margin-top', '22.5px');
} 

if($('#desktop .inner').find('>a').length === 7) {
    $('.conversation-content .inner-item').css('width', '150px').css('height', '158px');
    $('.conversation-content .inner-item .flag').css('width', '60px').css('height', '60px');
    $('.soon .coming-soon').css('width', '60px').css('height', '60px');
    $('.conversation-lang').css('font-size', '15px').css('margin-top', '21.5px');
} 

if($('#desktop .inner').find('>a').length === 8) {
    $('.conversation-content .inner-item').css('width', '130px').css('height', '137px');
    $('.conversation-content .inner-item .flag').css('width', '50px').css('height', '50px');
    $('.soon .coming-soon').css('width', '50px').css('height', '50px');
    $('.conversation-lang').css('font-size', '13.5px').css('margin-top', '20.5px');
} 

if($('#desktop .inner').find('>a').length === 9) {
    $('.conversation-content .inner-item').css('width', '118px').css('height', '124px');
    $('.conversation-content .inner-item .flag').css('width', '43px').css('height', '43px');
    $('.soon .coming-soon').css('width', '43px').css('height', '43px');
    $('.conversation-lang').css('font-size', '12.5px').css('margin-top', '18.5px');
} 

if($('#desktop .inner').find('>a').length === 10) {
    $('.conversation-content .inner-item').css('width', '100px').css('height', '108px');
    $('.conversation-content .inner-item .flag').css('width', '38px').css('height', '38px');
    $('.soon .coming-soon').css('width', '38px').css('height', '38px');
    $('.conversation-lang').css('font-size', '12px').css('margin-top', '16.5px');
} 


// conversation-languages carousel
$('.owl-one').owlCarousel({
    items:3,
    loop:true,
    dots: false,
    autoplay:false,
    autoplayTimeout:3000,
    autoplayHoverPause:true
});


// toggle-hamburger
var closedHamburger = true;
$('.hamburger-item').on('click', function () {
    if (closedHamburger == true) {
        $('.hamburger').css('transform', 'translate3d(0, 0, 0)').css('transition', '0.5s');
        $('.hamburger-item').addClass('open');
        closedHamburger = false;
    } else {
        $('.hamburger').css('transform', 'translate3d(-20px, 0, 0)').css('transition', '0.2s');
        setTimeout(() => {
            $('.hamburger').css('transform', 'translate3d(120vw, 0, 0)').css('transition', '0.3s');
        }, 200);
        $('.hamburger-item').removeClass('open');
        closedHamburger = true;
    }
});



// most recommend plan
$('.price #most').append('<div class="most-title">MOST RECOMMEND</div>');
$('.price #most').css('border', '1px solid #3D82F0');

// mobile-prices
$('.owl-two').owlCarousel({
    center: true,
    items:1,
    loop:false,
    margin: -120,
    dots: false, 
    responsive: {
        0: {
            margin: -120,
        }, 768: {
            items: 2,
            center: false,
            margin: 10,
        }   
    }
});

// blog carousel (homepage)
// ------
$('.blog-content .next').on('click', function () {
    $('.blog-content .blog-carousel .carousel-group:nth-child(1)').css('transform', 'translate(-1154px, 0)').css('transition', 'all 0.3s');
    $('.blog-content .blog-carousel .carousel-group:nth-child(2)').css('transform', 'translate(-1154px, 0)').css('transition', 'all 0.3s');
});

$('.blog-content .prev').on('click', function () {
    $('.blog-content .blog-carousel .carousel-group:nth-child(1)').css('transform', 'translate(0, 0)').css('transition', 'all 0.3s');
    $('.blog-content .blog-carousel .carousel-group:nth-child(2)').css('transform', 'translate(1154px, 0)').css('transition', 'all 0.3s');
});
// ------
$('.owl-three').owlCarousel({
    items:2,
    loop:false,
    margin: 10,
    nav: true,
    navText:[$('.carousel-buttons .prev'), $('.carousel-buttons .next')],
    dots: false,
    responsive: {
        0: {
            items: 1
        }, 768: {
            items: 2
        }
    }
});

$('.owl-three .owl-nav').find('button').map(function () {
    $(this).appendTo('.blog-content .carousel-buttons');});
// ------------------------------------------------------

var $socialHomeSlider = $(".social-carousel");

$(window).resize(function() {
  showSocialHomeSlider();
});

function showSocialHomeSlider() {
  if ($socialHomeSlider.data("owl-carousel") !== "undefined") {
    if (window.matchMedia('(max-width: 767px)').matches) {
      initialSocialHomeSlider();
    } else {
        destroySocialHomeSlider();
    }
  }
}
showSocialHomeSlider();

function initialSocialHomeSlider() {
  $socialHomeSlider.addClass("owl-carousel").owlCarousel({
    items:1,
    loop:false,
    margin: 0,
    dots: false,
    center: true,
    responsive: {
        0: {
            items: 2,
            margin: 90,
            center: true
        }, 1200: {
            items: 3
        }
    }
  });
}

function destroySocialHomeSlider() {
  $socialHomeSlider.trigger("destroy.owl.carousel").removeClass("owl-carousel");
}

// login
$('#login, #m-login').on('click', function () {
    $('.login-popup').show(350).css('display', 'flex');
    $('.hamburger').css('transform', 'translate3d(-20px, 0, 0)').css('transition', '0.2s');
    setTimeout(() => {
        $('.hamburger').css('transform', 'translate3d(120vw, 0, 0)').css('transition', '0.3s');
    }, 200);
    closedHamburger = true;
    $('.hamburger-item').removeClass('open');
});

$(document).click(function(event) {
    if (event.target.className === 'login-popup') {
        $('.login-popup').hide(350);
    }
});

// reset
$('#reset').on('click', function () {
    $('.login-popup').hide(350);
    $('.reset-popup').show(350).css('display', 'flex');
});


$(document).click(function(event) {
    if (event.target.className === 'reset-popup') {
        $('.reset-popup').hide(350);
    }
});

// sign
$('#signup, #m-signup').on('click', function () {
    $('.sign-popup').show(350).css('display', 'flex');
    $('.hamburger').css('transform', 'translate3d(-20px, 0, 0)').css('transition', '0.2s');
    setTimeout(() => {
        $('.hamburger').css('transform', 'translate3d(120vw, 0, 0)').css('transition', '0.3s');
    }, 200);
    closedHamburger = true;
    $('.hamburger-item').removeClass('open');    
});

$(document).click(function(event) {
    if (event.target.className === 'sign-popup') {
        $('.sign-popup').hide(350);
    }
});

// referer 
$('.login-ref').on('click', function () {
    $('.login-popup').hide(350);
    $('.sign-popup').show(350).css('display', 'flex');
});

$('.sign-ref').on('click', function () {
    $('.sign-popup').hide(350);
    $('.login-popup').show(350).css('display', 'flex');
});

$('.reset-ref').on('click', function () {
    $('.reset-popup').hide(350);
    $('.login-popup').show(350).css('display', 'flex');
});

});


$('.video').on('click', function() {
    var $element_id = $(this).find('.play-button').next().attr('id');

    $('.youtube-modal').find('iframe').attr('src', 'https://www.youtube.com/embed/' + $element_id + '?autoplay=1&enablejsapi=1');
    $('.youtube-modal').fadeIn(200).css('display', 'flex');
});
})();

$('.youtube-modal .close-modal').on('click', function() {
        $('.youtube-modal').fadeOut(200);
        $('.youtube-modal').find('iframe').attr('src', '');
});

function getEmbedThumbnail () {
    $video_count = $('.video').parent().length;

    for (var i = 0; i < $video_count; i++) {
        var $thumbnail = $('.video').eq(i).find('.video-thumbnail');
        var $id = $thumbnail.attr('id');
        $url = "http://img.youtube.com/vi/" + $id + "/0.jpg";
        $thumbnail.css('background', 'url( ' + $url + ' )');
    }
}


getEmbedThumbnail();


